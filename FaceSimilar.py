# Description : This code purpose is to delete similar photo from a directory
# with Hamming Distance methode.
# Contributor : Thariq Ramadhan
# Source : https://github.com/moondra2017/Computer-Vision

# Library Definition
import os
import itertools
from imageio import imread
import cv2
from scipy.spatial import distance
import numpy as np

# VARIABEL # Can be change
# Directory
DIR = 'D:\Riset_ai\Anisa Rahma'
# Hamming distance threshold
THRESHOLD = 0.33

def main():
    os.chdir(DIR)
    image_files = os.listdir()

    image_files = filter_images(image_files)
    duplicates, ds_dict = difference_score_dict(image_files)

    for k1,k2 in itertools.combinations(ds_dict, 2):
        if hamming_distance(ds_dict[k1], ds_dict[k2])< THRESHOLD:
            duplicates.append((k1,k2))

    for file_names in duplicates:
        try:
            os.remove(file_names[0])
        except:
            pass

def filter_images(images):
    image_list = []
    for image in images:
        try:
            assert imread(image).shape[2] == 3
            image_list.append(image)
        except  AssertionError as e:
            print(e)
    return image_list

def difference_score_dict(image_list):
    ds_dict = {}
    duplicates = []
    for image in image_list:
        ds = difference_score(image)
        
        if image not in ds_dict:
            ds_dict[image] = ds
        else:
            duplicates.append((image, ds_dict[image]) )
    return  duplicates, ds_dict

def difference_score(image, height = 30, width = 30):
    gray = img_gray(image)
    row_res, col_res = resize(gray, height, width)
    difference = intensity_diff(row_res, col_res)   
    return difference

def img_gray(image):
    image = imread(image)
    return np.average(image, weights=[0.299, 0.587, 0.114], axis=2)

def resize(image, height=30, width=30):
    row_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten()
    col_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten('F')
    return row_res, col_res

def intensity_diff(row_res, col_res):
    difference_row = np.diff(row_res)
    difference_col = np.diff(col_res)
    difference_row = difference_row > 0
    difference_col = difference_col > 0
    return np.vstack((difference_row, difference_col)).flatten()

def hamming_distance(image, image2):
    score = distance.hamming(image, image2)
    return score


if __name__ == "__main__":
    main()
